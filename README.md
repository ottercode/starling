# Starling DAW

Starling is a free-of-cost and open-source Digital Audio Workstation, or DAW for short.
It is in a very early stage of development, and is not yet usable for music production. (Bear with us, we're working on it.)

The codebase is very young, and is rapidly growing and changing, so communication between contributors is especially important.
For this reason, if you would like to contribute to the project, please contact Frank at ottercode@yahoo.com

## Building

Starling should build with any GCC which supports C++17. MSVC has not been tested.

Install the required dependencies:

* cmake
* Qt5 framework 5.13 or later
* libsoundio [2.0.0](https://github.com/andrewrk/libsoundio/releases/tag/2.0.0) or later

Then, from within the repository directory, run the following commands to build Starling:

```
mkdir build
cd build
cmake ..
make starling
```

The executable will be placed within `build/src`

### A note about libsoundio:

libsoundio 2.0 is currently only available as source code. Therefore, you will need to follow the instructions
in libsoundio's `README.md` in order to build and install libsoundio.

## License

The program as a whole is licensed under the GNU General Public License Version 3 (GPLv3), which can be found in the COPYING file.
Some individual files may be licensed under more permissive licenses, such as LGPL or MIT, which are "one-way compatible" with GPLv3;
that is, these files can be included in a project licensed under GPLv3, but files licensed under GPLv3 cannot be included in a
project licensed under one of those licenses.
