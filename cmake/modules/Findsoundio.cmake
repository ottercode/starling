# - Find libsoundio
#
# This module defines
#  SOUNDIO_FOUND
#  SOUNDIO_LIBRARIES
#  SOUNDIO_INCLUDE_DIR
#

if (SOUNDIO_LIBRARIES AND SOUNDIO_INCLUDE_DIR)
    # Already in cache
    SET (SOUNDIO_FOUND TRUE)
else ()
    FIND_LIBRARY (SOUNDIO_LIBRARIES soundio
            /usr/lib
            /usr/local/lib
            PATH_SUFFIXES soundio
    )

    FIND_PATH (SOUNDIO_INCLUDE_DIR soundio.h
            /usr/include
            /usr/local/include
            ${CMAKE_INCLUDE_PATH}
            PATH_SUFFIXES soundio
    )

    if (SOUNDIO_LIBRARIES AND SOUNDIO_INCLUDE_DIR)
        set (SOUNDIO_FOUND TRUE)
    else ()
        set (SOUNDIO_FOUND FALSE)
    endif ()
endif ()