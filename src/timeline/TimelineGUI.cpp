/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TimelineGUI.h"
#include <QQuickWidget>
#include <QQuickItem>
#include "ClipLane.h"
#include "MIDIQuickView.h"
#include <audioengine/AudioEngine.h>
#include <qvariant.h>

TimelineGUI::TimelineGUI(QQuickWidget* widget, AudioEngine* engine) : QObject(widget)
{
    mWidget = widget;
    mAudioEngine = engine;

    // load the layout
    mWidget->setSource(QUrl::fromLocalFile(":/qml/TimelineArea.qml"));

    // obtain references from the layout
    mContext = mWidget->rootContext();
    mLaneContainer = qobject_cast<QQuickItem*>( mWidget->rootObject()->findChild<QObject*>("leftContainer"));
    mLaneHeaderContainer = qobject_cast<QQuickItem*>( mWidget->rootObject()->findChild<QObject*>("rightContainer"));

    // load components
    mLaneComponent = new QQmlComponent(mWidget->engine(), QUrl::fromLocalFile(":/qml/LaneComponent.qml"), this);
    mLaneHeaderComponent = new QQmlComponent(mWidget->engine(), QUrl::fromLocalFile(":/qml/LaneHeaderComponent.qml"), this);
    mClipComponent = new QQmlComponent(mWidget->engine(), QUrl::fromLocalFile(":/qml/ClipComponent.qml"), this);
}

void TimelineGUI::makeClip(ClipLane *track, double startTime, double endTime)
{
    if(endTime < 0)
        endTime = startTime + 4;

    track->makeClip(startTime, endTime).get();
}

void TimelineGUI::addClip(ClipLane* track, MIDIClip* clip)
{
    auto& elements = mLaneElementMap[track];
    
    QQuickItem* clipItem = qobject_cast<QQuickItem*>(mClipComponent->create(mContext));
    clipItem->setProperty("_backend", QVariant::fromValue(clip));
    clipItem->setParentItem(elements.laneItem);
    connect(clip, &MIDIClip::requestEditor, this, &TimelineGUI::openEditor);

    auto* contentContainer = qobject_cast<QQuickItem*>(clipItem->findChild<QObject*>("contentContainer"));
    auto* content = new MIDIQuickView(clip, contentContainer);
    //content->setProperty("anchors.fill", "parent");
}

void TimelineGUI::objectDestroyed(QObject* object)
{
    if(ClipLane* track = qobject_cast<ClipLane*>(object)) {
        mLaneElementMap.remove(track);
    }
}

void TimelineGUI::addMIDITrack()
{
    // configure the clip lane, add it to the audio engine
    auto clipLane = std::make_shared<ClipLane>(nullptr);
    connect(clipLane.get(), &AudioModuleChain::requestEditor, this, &TimelineGUI::openEditor);
    connect(clipLane.get(), &ClipLane::destroyed, this, &TimelineGUI::objectDestroyed);
    connect(clipLane.get(), &ClipLane::clipCreated, this, &TimelineGUI::addClip);
    mAudioEngine->addChain(clipLane);

    // create the lane and header GUI elements
    QQuickItem* laneItem = qobject_cast<QQuickItem*>(mLaneComponent->create(mContext));
    QQuickItem* laneHeaderItem = qobject_cast<QQuickItem*>(mLaneHeaderComponent->create(mContext));

    // configure the lane GUI element
    laneItem->setParentItem(mLaneContainer);
    laneItem->setProperty("_header", QVariant::fromValue(laneHeaderItem));
    laneItem->setProperty("_backend", QVariant::fromValue(this));
    laneItem->setProperty("_signalChain", QVariant::fromValue(clipLane.get()));
    connect(clipLane.get(), &QObject::destroyed, laneItem, &QObject::deleteLater);

    // configure the header GUI element
    laneHeaderItem->setParentItem(mLaneHeaderContainer);
    laneHeaderItem->setProperty("_lane", QVariant::fromValue(laneItem));
    laneHeaderItem->setProperty("_backend", QVariant::fromValue(this));
    laneHeaderItem->setProperty("_signalChain", QVariant::fromValue(clipLane.get()));
    connect(clipLane.get(), &QObject::destroyed, laneHeaderItem, &QObject::deleteLater);

    // add to the map
    mLaneElementMap.insert(clipLane.get(), {laneItem, laneHeaderItem});
}
