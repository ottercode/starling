/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIMELINE_GUI_H
#define TIMELINE_GUI_H

#include <QObject>
#include <QMap>

class QQuickWidget;
class QQuickItem;
class QQmlComponent;
class AudioEngine;
class QQmlContext;
class ClipLane;
class MIDIClip;

/**
    TimelineGUI encapsulates the GUI elements of the timeline. It is not
    a widget or item in and of itself, but manages the creation of, and
    interractions between, the items which make up the timeline GUI, as
    well as the interractions between the timeline GUI and the backend.

    In the LaneComponent and LaneHeaderComponent QML components, _backend
    is an instance of this TimelineGUI.
*/
class TimelineGUI : public QObject
{
    Q_OBJECT

public:
    TimelineGUI(QQuickWidget* widget, AudioEngine* engine);

public slots:
    void addMIDITrack();
    Q_INVOKABLE void makeClip(ClipLane* track, double startTime = 0, double endTime = -1);
    void addClip(ClipLane* track, MIDIClip* clip);
    void objectDestroyed(QObject* object);

signals:
    void openEditor(QObject* target);

private:
    struct LaneGUIElements
    {
        QQuickItem* laneItem;
        QQuickItem* headerItem;
    };

    AudioEngine* mAudioEngine;
    QQuickWidget* mWidget = nullptr;
    QQmlContext* mContext;
    QQuickItem* mLaneContainer;
    QQuickItem* mLaneHeaderContainer;
    QQmlComponent* mLaneComponent;
    QQmlComponent* mLaneHeaderComponent;
    QQmlComponent* mClipComponent;
    
    QMap<ClipLane*, LaneGUIElements> mLaneElementMap;
};

#endif // TIMELINE_GUI_H