/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ClipLane.h"
#include <vector>

ClipLane::ClipLane(QObject* parent) : AudioModuleChain(parent)
{
    
}

void ClipLane::generateSamples(StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI)
{
    std::shared_lock lock(mMutex);

    std::vector<MIDIWord> midiWords;
    std::vector<uint64_t> midiOffsets;

    // TODO: combine MIDI events from multiple clips when relevant
    if(mIsPlaybackOn)
        for(auto& clip : mClips)
        {
            clip->getMIDIEvents(midiWords, midiOffsets, sampleRate, mCurrentTime, mCurrentTime+(numSamples/sampleRate));
            if(midiWords.size() > 0)
                break;
        }

    MIDISequence midiSequence;
    midiSequence.words = midiWords.data();
    midiSequence.offsets = midiOffsets.data();
    midiSequence.numWords = midiWords.size();
    midiSequence.numOffsets = midiOffsets.size();

    // generate samples
    AudioModuleChain::generateSamples(outBuffers, sampleRate, numSamples, &midiSequence);

    if(mIsPlaybackOn)
        mCurrentTime += numSamples / sampleRate;
}

void ClipLane::seek(double time)
{
    std::unique_lock lock(mMutex);

    mCurrentTime = 0;
}

void ClipLane::setPlaybackOn(bool playback)
{
    mIsPlaybackOn = playback;
}

std::shared_ptr<MIDIClip> ClipLane::makeClip(double startTime, double endTime)
{
    std::unique_lock lock(mMutex);

    std::shared_ptr<MIDIClip> clip = std::make_shared<MIDIClip>();
    clip->setEndTime(endTime);
    clip->setStartTime(startTime);
    mClips.emplace_back(clip);

    emit clipCreated(this, clip.get());

    return clip;
}

void ClipLane::removeClip(MIDIClip *clip)
{
    std::unique_lock lock(mMutex);

    size_t numClips = mClips.size();
    for(size_t i=0; i<numClips; i++)
        if(mClips[i].get() == clip)
        {
            mClips.erase(std::next(mClips.begin(), i));
            break;
        }
}
