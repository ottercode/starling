/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MIDI_QUICK_VIEW_H
#define MIDI_QUICK_VIEW_H

#include <QQuickPaintedItem>
#include <audiocommon/MIDIClip.h>

/**
    MIDIQuickView visually renders the MIDI notes within a clip on the timeline.
*/
class MIDIQuickView: public QQuickPaintedItem
{
    Q_OBJECT

public:
    MIDIQuickView(MIDIClip* clip, QQuickItem* parent = nullptr);
    void paint(QPainter* painter) override;

private:
    MIDIClip* mClip;
};

#endif // MIDI_QUICK_VIEW_H
