/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MIDIQuickView.h"
#include <QQuickPaintedItem>
#include <QPainter>

MIDIQuickView::MIDIQuickView(MIDIClip* clip, QQuickItem* parent) : QQuickPaintedItem(parent)
{
    mClip = clip;
    this->setParentItem(parent);
    qvariant_cast<QObject*>(this->property("anchors"))->setProperty("fill", this->property("parent"));
    connect(mClip, &QObject::destroyed, this, &QObject::deleteLater);
    connect(mClip, &MIDIClip::notesChanged, this, &QQuickItem::update);
}

void MIDIQuickView::paint(QPainter* painter)
{
    painter->save();

    // render notes
    // TODO: make this prettier
    auto notes = mClip->getNotes();
    if(notes->size() > 0)
    {
        bool wasReal = true;
        double pixelsPerBeat = qobject_cast<QQuickItem*>(parent())->property("timeScale").toReal(&wasReal);
        if(!wasReal)
            return; // could not get time scale

        uint8_t lowestNoteValue = 255;
        uint8_t highestNoteValue = 0;

        // find lowest and highest value
        for(auto& note : *notes)
        {
            uint8_t value = note.second.note;
            if(value > highestNoteValue)
                highestNoteValue = value;
            if(value < lowestNoteValue)
                lowestNoteValue = value;
        }

        double valueHeight = (double)height() / (double)(highestNoteValue - lowestNoteValue + 1);
        painter->setRenderHint(QPainter::Antialiasing, false);

        // draw each note as a rectangle
        for(auto& note : *notes)
        {
            int left = note.second.onTime * pixelsPerBeat;
            int width = (note.second.offTime * pixelsPerBeat) - left;
            int top = (highestNoteValue - note.second.note) * valueHeight;
            int height = (highestNoteValue - note.second.note + 1) * valueHeight - top;
            painter->fillRect(left, top, width, height, Qt::black);
        }
    }

    painter->restore();
}
