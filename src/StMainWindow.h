/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HELLO_WINDOW_H
#define HELLO_WINDOW_H

#include <QMainWindow>

class AudioEngine;
class TimelineGUI;

namespace Ui
{
    class StMainWindow;
}

/**
    StMainWindow is the main window of the Starling DAW. It provides an
    overall layout which other interfaces fit into, and connects them
    together using signals and slots.
 */
class StMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StMainWindow(QWidget* parent = 0);
    ~StMainWindow();

public slots:
    void openEditor(QObject* target);
    void playButtonPressed();
    void stopButtonPressed();
    void objectDestroyed(QObject* object = nullptr);

private:
    Ui::StMainWindow *ui;
    AudioEngine* mAudioEngine;
    QObject* mCurrentLowerEditor = nullptr;
    TimelineGUI* mTimelineGUI = nullptr;
};

#endif // HELLO_WINDOW_H