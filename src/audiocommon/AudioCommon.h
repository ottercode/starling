/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUDIO_COMMON_H
#define AUDIO_COMMON_H

#include <vector>

typedef double StSample;

enum StChannelType
{
    ST_CHANNEL_LEFT,
    ST_CHANNEL_RIGHT,
    ST_CHANNEL_MONO,
    ST_CHANNEL_OTHER,
    ST_MAX_ENUM
};

struct StBuffers
{
    std::vector<StSample*> buffers;
    std::vector<StChannelType> channelTypes;
};

#endif // AUDIO_COMMON_H