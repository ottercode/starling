/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MIDIClip.h"
#include <mutex>
#include <utility>

inline bool isBetween(double lower, double middle, double upper)
{
    return (lower <= middle) && (middle <= upper);
}

MIDIClip::MIDIClip()
{
    addNote({76, 0.0, 0.5});
    addNote({81, 1.0, 1.5});
    addNote({83, 0.75, 1.0});
}

MIDIClip::~MIDIClip()
{
    std::shared_lock lock(mMutex);
}

MIDIClip::MIDIClip(const MIDIClip& other)
{
    // use operator=
    *this = other;
}

MIDIClip& MIDIClip::operator=(const MIDIClip& other)
{
    mNextNoteIndex = other.mNextNoteIndex;
    mNotes = other.mNotes;
    mNoteMap = other.mNoteMap;
    mStartTime = other.mStartTime;
    mEndTime = other.mEndTime;

    return *this;
}

bool MIDIClip::activeAtTime(double time)
{
    std::shared_lock lock(mMutex);

    return isBetween(mStartTime, time, mEndTime);
}

void MIDIClip::getMIDIEvents(std::vector<MIDIWord>& words, std::vector<uint64_t>& offsets, double sampleRate, double tStart, double tEnd)
{
    std::shared_lock lock(mMutex);

    words = {};
    offsets = {};

    if(tEnd < mStartTime || tStart > mEndTime)
        return;

    double relativeStart = tStart - mStartTime;
    double relativeEnd = tEnd - mStartTime;

    auto noteRef = mNoteMap.lower_bound(relativeStart);
    auto endRef = mNoteMap.upper_bound(relativeEnd);
    while(noteRef != endRef)
    {
        auto& time = noteRef->first;
        auto& note = mNotes[noteRef->second];

        // TODO: implement more detailed, MIDI 2.0 messages
        MIDIWord word;
        word.bytes[0] = 0x20;
        word.bytes[1] = 0x90;
        word.bytes[2] = note.note;
        word.bytes[3] = 64;

        if(time == note.onTime) // note on
            word.bytes[1] = 0x90;
        else                    // note off
            word.bytes[1] = 0x80;

        words.push_back(word);
        offsets.push_back((time-relativeStart)*sampleRate);

        noteRef++;
    }
}

const std::unordered_map<uint64_t, MIDIClip::Note>* MIDIClip::getNotes()
{
    return &mNotes;
}

uint64_t MIDIClip::addNote(Note note)
{
    std::unique_lock lock(mMutex);
    size_t index = mNextNoteIndex;

    mNotes.emplace(mNextNoteIndex, note);
    mNoteMap.insert(std::pair<double, size_t>(note.onTime, index));
    mNoteMap.insert(std::pair<double, size_t>(note.offTime, index));
    mNextNoteIndex++;

    emit notesChanged();

    return index;
}

void MIDIClip::removeNote(uint64_t index)
{
    std::unique_lock lock(mMutex);

    removeFromNoteMap(index);
    mNotes.erase(index);

    emit notesChanged();
}

void MIDIClip::editNote(uint64_t index, Note &newNote)
{
    std::unique_lock lock(mMutex);

    removeFromNoteMap(index);
    mNotes[index] = newNote;
    mNoteMap.insert(std::pair<double, size_t>(newNote.onTime, index));
    mNoteMap.insert(std::pair<double, size_t>(newNote.offTime, index));

    emit notesChanged();
}

// TODO: signal note end(s) when this happens during playback
void MIDIClip::setStartEndTime(double start, double end)
{
    std::unique_lock lock(mMutex);

    mStartTime = start;
    mEndTime = end;
}

double MIDIClip::startTime()
{
    return mStartTime;
}

double MIDIClip::endTime()
{
    return mEndTime;
}

void MIDIClip::setStartTime(double start)
{
    if(mStartTime == start)
        return;

    mStartTime = start;
    if(mStartTime < 0)
        mStartTime = 0;
    emit startTimeChanged(mStartTime);
}

void MIDIClip::setEndTime(double end)
{
    if(mEndTime == end)
        return;

    mEndTime = end;
    emit endTimeChanged(end);
}

void MIDIClip::openEditor()
{
    emit requestEditor(this);
}

void MIDIClip::rebuildNoteMap()
{
    mNoteMap.clear();
    for(size_t i=0; i<mNotes.size(); i++)
    {
        auto& note = mNotes[i];
        mNoteMap.insert(std::pair<double, size_t>(note.onTime, i));
        mNoteMap.insert(std::pair<double, size_t>(note.offTime, i));
    }
}

void MIDIClip::removeFromNoteMap(uint64_t index)
{
    auto& note = mNotes[index];
    auto first = mNoteMap.lower_bound(note.onTime);
    auto last = mNoteMap.upper_bound(note.onTime);
    for(; first != last; first++)
        if(first->second == index)
        {
            mNoteMap.erase(first);
            break;
        }

    first = mNoteMap.lower_bound(note.offTime);
    last = mNoteMap.upper_bound(note.offTime);
    for(; first != last; first++)
        if(first->second == index)
        {
            mNoteMap.erase(first);
            break;
        }
}
