/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MIDI.h"

MIDIParser::MIDIParser(const MIDIWord* words, size_t numWords, const uint64_t* offsets, size_t numOffsets)
{
    mWords = words;
    mWordsLeft = numWords;
    mOffsets = offsets;
    mOffsetsLeft = numOffsets;

    if(words == nullptr)
        mWordsLeft = 0;
    if(offsets == nullptr)
        mOffsetsLeft = 0;
}

MIDIParser::MIDIParser(const MIDISequence& sequence) : MIDIParser(sequence.words, sequence.numWords, sequence.offsets, sequence.numOffsets){}

uint8_t MIDIParser::mt()
{
    return mWords[0].bytes[0] >> 4;
}

size_t MIDIParser::packetSize()
{
    switch(mWords[0].bytes[0] >> 4)
    {
    case 0x0: case 0x1: case 0x2: case 0x6: case 0x7:
        return 1;
    case 0x3: case 0x4: case 0x8: case 0x9: case 0xA:
        return 2;
    case 0xB: case 0xC:
        return 3;
    case 0xD: case 0xE: case 0xF:
        return 4;
    }
    // this is impossible, but do it anyway
    return 0;
}

bool MIDIParser::isNoteOn()
{
    uint8_t mt_ = mt();
    if(mt_ == 0x2 || mt_ == 0x4)
    {
        uint8_t status_ = mWords[0].bytes[1] >> 4;
        return (status_ == 0x9);
    }
    else
        return false;
}

bool MIDIParser::isNoteOff()
{
    uint8_t mt_ = mt();
    if(mt_ == 0x2 || mt_ == 0x4)
    {
        uint8_t status_ = mWords[0].bytes[1] >> 4;
        return (status_ == 0x8);
    }
    else
        return false;
}

uint16_t MIDIParser::velocity()
{
    uint8_t mt_ = mt();
    if(mt_ == 0x2)
        return mWords[0].bytes[2] * 256;
    else if(mt_ == 0x4)
        return mWords[1].bytes[0] * 256 + mWords[1].bytes[1];
    else
        return 0;
}

uint8_t MIDIParser::note()
{
    uint8_t mt_ = mt();
    if(mt_ == 0x2 || mt_ == 0x4)
        return mWords[0].bytes[2];
    else
        return 0xFF;
}

bool MIDIParser::next()
{
    if(mWordsLeft == 0)
        return false;
    
    if(isFirstWord)
    {
        isFirstWord = false;
        return true;
    }

    size_t psize_ = packetSize();
    if(mWordsLeft <= psize_)
        return false;

    mWords += psize_;
    mWordsLeft -= psize_;
    if(mOffsetsLeft > 0)
    {
        mOffsets++;
        mOffsetsLeft--;
    }
    return true;
}

bool MIDIParser::next(size_t offset)
{
    if(mOffsets == nullptr)
        return next();
    else if(mOffsetsLeft == 0)
        return false;
    else if(offset >= *mOffsets)
        return next();

    return false;
}
