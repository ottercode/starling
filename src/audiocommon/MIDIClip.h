/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MIDI_CLIP_H
#define MIDI_CLIP_H

#include <vector>
#include <map>
#include <shared_mutex>
#include <unordered_map>
#include <QObject>
#include "MIDI.h"

class ClipLane;

/**
    MIDIClip encapsulates the MIDI data contained within a "clip"
    on the timeline, and can generate MIDI events for a given
    stretch of time if it is active during that time.
 */
class MIDIClip : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qreal startTime READ startTime WRITE setStartTime NOTIFY startTimeChanged)
    Q_PROPERTY(qreal endTime READ endTime WRITE setEndTime NOTIFY endTimeChanged)

public:
    struct Note
    {
        uint8_t note = 69;
        double onTime = 0.0;
        double offTime = 0.1;
    };

    MIDIClip();
    ~MIDIClip();
    MIDIClip(const MIDIClip& other);
    MIDIClip& operator=(const MIDIClip& other);

    bool activeAtTime(double time);
    void getMIDIEvents(std::vector<MIDIWord>& words, std::vector<uint64_t>& offsets, double sampleRate, double tStart, double tEnd);
    const std::unordered_map<uint64_t, Note>* getNotes();
    uint64_t addNote(Note note);
    void removeNote(uint64_t index);
    void editNote(uint64_t index, Note& newNote);
    void setStartEndTime(double start, double end);
    double startTime();
    double endTime();
    void setStartTime(double start);
    void setEndTime(double end);
    Q_INVOKABLE void openEditor();

signals:
    void startTimeChanged(double start);
    void endTimeChanged(double end);
    void requestEditor(MIDIClip* clip);
    void notesChanged();

private:
    void rebuildNoteMap();
    void removeFromNoteMap(uint64_t index);

    uint64_t mNextNoteIndex = 0;
    std::unordered_map<uint64_t, Note> mNotes;
    std::multimap<double, size_t> mNoteMap;
    std::shared_mutex mMutex;
    double mStartTime = 0.0;
    double mEndTime = 3.0;
};

#endif // MIDI_CLIP_H