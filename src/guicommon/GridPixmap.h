/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 */

#ifndef GRID_PIXMAP_H
#define GRID_PIXMAP_H

#include <QPixmap>

/**
    Creates a QPixmap which makes up one cell of a grid. These pixmaps
    can be tiled automatically by Qt's rendering engine to create a grid.
 */
const QPixmap createGridPixmap(uint32_t width, uint32_t height, const QColor& hColor, const QColor& vColor);

#endif // GRID_PIXMAP_MAKER_H