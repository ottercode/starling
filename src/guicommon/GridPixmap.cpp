/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 */

#include "GridPixmap.h"
#include <QPainter>

const QPixmap createGridPixmap(uint32_t width, uint32_t height, const QColor& hColor, const QColor& vColor)
{
    QPixmap pixmap(width, height);
    QPainter painter;

    pixmap.fill(Qt::transparent);
    painter.begin(&pixmap);
    painter.setPen(hColor);
    painter.drawLine(0, 0, width, 0);
    painter.setPen(vColor);
    painter.drawLine(0, 0, 0, height);

    return pixmap;
}