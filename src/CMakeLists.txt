add_executable(starling
    main.cpp
    StMainWindow.cpp
    StMainWindow.h
    qml/qml.qrc
)

include_directories(.)

add_subdirectory(timeline EXCLUDE_FROM_ALL)
add_subdirectory(audioengine EXCLUDE_FROM_ALL)
add_subdirectory(audiomodules EXCLUDE_FROM_ALL)
add_subdirectory(audiocommon EXCLUDE_FROM_ALL)
add_subdirectory(midiedit EXCLUDE_FROM_ALL)
add_subdirectory(guicommon EXCLUDE_FROM_ALL)

target_link_libraries(starling PRIVATE Qt5::Widgets Qt5::Quick Qt5::Qml Qt5::QuickWidgets st_timeline st_audioengine st_audiomodules st_audiocommon st_midiedit st_guicommon)
