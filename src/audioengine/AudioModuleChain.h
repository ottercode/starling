/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
#ifndef AUDIO_MODULE_CHAIN_H
#define AUDIO_MODULE_CHAIN_H

#include <vector>
#include <memory>
#include <mutex>
#include <QObject>
#include <audiocommon/AudioModule.h>

/**
    AudioModuleChain encapsulates one full signal chain, composed of
    AudioModules which perform signal processing one after the other.
    An AudioModuleChain may have one "instrument" at the beginning,
    which takes in MIDI information and produces sound, or it may
    contain only "effects", which take in sound, process it, and
    produce output sound. AudioModuleChain is intended to be subclassed
    to provide specialized functionality, although the default
    implementation is sufficient to generate sound samples.
*/
class AudioModuleChain : public QObject
{
    Q_OBJECT

public:
    // Public Functionality
    AudioModuleChain(QObject* parent = nullptr);
    std::vector<std::shared_ptr<AudioModule>> getModules();
    void updateModules(std::vector<std::shared_ptr<AudioModule>> newModules);
    virtual void generateSamples(StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI);
    virtual void seek(double time);
    virtual void setPlaybackOn(bool playback);
    Q_INVOKABLE void openEditor();

signals:
    void requestEditor(AudioModuleChain* chain);

private:
    std::vector<std::shared_ptr<AudioModule>> mModules;
    std::mutex mModuleMutex;
};

#endif // AUDIO_MODULE_CHAIN_H