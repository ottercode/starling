/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AudioModuleChain.h"
#include <cstdint>
#include <mutex>

AudioModuleChain::AudioModuleChain(QObject* parent) : QObject(parent)
{
    
}

std::vector<std::shared_ptr<AudioModule>> AudioModuleChain::getModules()
{
    const std::lock_guard<std::mutex> lock(mModuleMutex);
    return mModules;
}

void AudioModuleChain::updateModules(std::vector<std::shared_ptr<AudioModule>> newModules)
{
    const std::lock_guard<std::mutex> lock(mModuleMutex);
    mModules = newModules;
}

void AudioModuleChain::generateSamples(StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI)
{
    // obtain a local copy of the modules vector
    mModuleMutex.lock();
    auto modules = this->mModules;
    mModuleMutex.unlock();

    size_t numChannels = outBuffers.buffers.size();

    // create temporary buffers
    std::vector<double> tempBufferData(numSamples * numChannels);
    StBuffers tempBuffers;
    tempBuffers.channelTypes = outBuffers.channelTypes;
    tempBuffers.buffers = std::vector<StSample*>(numChannels);
    for(size_t i=0; i<numChannels; i++)
        tempBuffers.buffers[i] = tempBufferData.data() + i*numSamples;

    // set up swappable buffer pointers
    StBuffers *buf1, *buf2;
    if((modules.size() & 1) == 0)
    {
        buf1 = &outBuffers;
        buf2 = &tempBuffers;
    }
    else
    {
        buf1 = &tempBuffers;
        buf2 = &outBuffers;
    }

    // perform processing for each module in the chain
    for(size_t i=0; i<modules.size(); i++)
    {
        modules[i]->generateSamples(*buf1, *buf2, sampleRate, numSamples, inMIDI);
        // swap buffers
        auto* temp = buf1;
        buf1 = buf2;
        buf2 = temp;
    }
}

void AudioModuleChain::seek(double time)
{
    // do nothing
}

void AudioModuleChain::setPlaybackOn(bool playback)
{
    // do nothing
}

void AudioModuleChain::openEditor()
{
    emit requestEditor(this);
}
