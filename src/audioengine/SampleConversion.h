/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SAMPLE_CONVERSION_H
#define SAMPLE_CONVERSION_H

#include <cstddef>
#include <cstdint>
#include <limits>
#include <type_traits>
#include <algorithm>
#include <cstring>
#include <soundio.h>

namespace impl
{

enum class Endianness
{
    LittleEndian,
    BigEndian
};

constexpr Endianness nativeEndianness()
{
#if defined(ST_IS_BIG_ENDIAN)
    return Endianness::BigEndian;
#elif defined(ST_IS_LITTLE_ENDIAN)
    return Endianness::LittleEndian;
#else
    static_assert(false, "system endianness is unknown");
#endif
}

#define _ST_FOREACH_FORMAT(FMT) \
    /* endianness doesn't matter for single-byte types */ \
    FMT(SoundIoFormatS8, int8_t, nativeEndianness(), 1)\
    FMT(SoundIoFormatU8, uint8_t, nativeEndianness(), 1)\
    FMT(SoundIoFormatS16LE, int16_t, Endianness::LittleEndian, 2)\
    FMT(SoundIoFormatS16BE, int16_t, Endianness::BigEndian, 2)\
    FMT(SoundIoFormatU16LE, uint16_t, Endianness::LittleEndian, 2)\
    FMT(SoundIoFormatU16BE, uint16_t, Endianness::BigEndian, 2)\
    /* 24-bit samples are 32 bits with the high byte truncated */ \
    FMT(SoundIoFormatS24LE, int32_t, Endianness::LittleEndian, 3)\
    FMT(SoundIoFormatS24BE, int32_t, Endianness::BigEndian, 3)\
    FMT(SoundIoFormatU24LE, uint32_t, Endianness::LittleEndian, 3)\
    FMT(SoundIoFormatU24BE, uint32_t, Endianness::BigEndian, 3)\
    FMT(SoundIoFormatS32LE, int32_t, Endianness::LittleEndian, 4)\
    FMT(SoundIoFormatS32BE, int32_t, Endianness::BigEndian, 4)\
    FMT(SoundIoFormatU32LE, uint32_t, Endianness::LittleEndian, 4)\
    FMT(SoundIoFormatU32BE, uint32_t, Endianness::BigEndian, 4)\
    FMT(SoundIoFormatFloat32LE, float, Endianness::LittleEndian, 4)\
    FMT(SoundIoFormatFloat32BE, float, Endianness::BigEndian, 4)\
    FMT(SoundIoFormatFloat64LE, double, Endianness::LittleEndian, 8)\
    FMT(SoundIoFormatFloat64BE, double, Endianness::BigEndian, 8)

#define _ST_DEFINE_SAMPLE_TRAITS(FORMAT, TYPE, ENDIANNESS, SIZE) \
    template <> \
    struct SampleFormatTraits<FORMAT> \
    { \
        using Type = TYPE; \
        constexpr static Endianness endianness = (ENDIANNESS); \
        constexpr static size_t size = (SIZE); \
    };

template <SoundIoFormat format>
struct SampleFormatTraits;

_ST_FOREACH_FORMAT(_ST_DEFINE_SAMPLE_TRAITS)
#undef _ST_DEFINE_SAMPLE_TRAITS

// pow(a, 0) = 1
// pow(a, b) = a * pow(a, b - 1)
template <auto base, auto exponent>
constexpr double integerPow()
{
    if constexpr (exponent == 0) {
        return 1.0f;
    } else {
        return base * integerPow<base, exponent - 1>();
    }
}

template <SoundIoFormat format>
inline typename SampleFormatTraits<format>::Type convertSample(double src) {
    using SampleType = typename SampleFormatTraits<format>::Type;

    // integers
    if constexpr (std::is_integral_v<SampleType>) {
        if constexpr (std::is_signed_v<SampleType>) {
            double sampleTypeMaximum = integerPow<2, SampleFormatTraits<format>::size * 8 - 1>() - 2.0;
            double sampleTypeMinimum = -integerPow<2, SampleFormatTraits<format>::size * 8 - 1>() + 2.0;
            return static_cast<SampleType>(src * (sampleTypeMaximum - sampleTypeMinimum) / 2.0);
        } else {
            // (src * 0.5 + 0.5) * max
            // = max * src * 0.5 + max * 0.5
            double sampleTypeMaximum = integerPow<2, SampleFormatTraits<format>::size * 8>() - 2.0;
            double halfMax = 0.5 * sampleTypeMaximum;
            return static_cast<SampleType>(halfMax * src + halfMax);
        }
    }

    // floats
    else if constexpr (std::is_arithmetic_v<SampleType>) {
        return static_cast<SampleType>(src);
    }
}

template <SoundIoFormat format>
inline void writeSamples(char *dest, size_t stride, const double *src, size_t numSamples)
{
    using FormatTraits = SampleFormatTraits<format>;

    for (size_t currentSample = 0; currentSample < numSamples; currentSample++)
    {
        char *currentDest = dest + stride * currentSample;

        // basically any sort of type punning is disallowed in C++, so we have to resort to using a
        // byte copy instead, but this seems well tolerated by the optimizer.
        auto castSample = convertSample<format>(src[currentSample]);
        char sampleBytes[sizeof(castSample)];
        std::memcpy(static_cast<char *>(sampleBytes), &castSample, sizeof(castSample));

        static_assert(FormatTraits::size <= sizeof(sampleBytes));
        for (int currentByte = 0; currentByte < FormatTraits::size; ++currentByte)
        {
            if constexpr (nativeEndianness() == FormatTraits::endianness) {
                currentDest[currentByte] = sampleBytes[currentByte];
            } else {
                currentDest[currentByte] = sampleBytes[sizeof(sampleBytes) - 1 - currentByte];
            }
        }
    }
}

} // namespace impl

template <SoundIoFormat format>
inline void writeSamples(char *dest, size_t stride, const double *src, size_t numSamples) {
    impl::writeSamples<format>(dest, stride, src, numSamples);
}

using writeSamplesPFN = void (*)(char *dest, size_t step, const double *src, size_t numSamples);

#define _ST_WRITE_FUNC_CASE(FORMAT, _0, _1, _2) case FORMAT: return &writeSamples<FORMAT>;
inline writeSamplesPFN selectSampleWriteFunc(SoundIoFormat format) {
    switch (format)
    {
    _ST_FOREACH_FORMAT(_ST_WRITE_FUNC_CASE)
    default: return nullptr;
    }
}
#undef _ST_WRITE_FUNC_CASE

#endif // SAMPLE_CONVERSION_H