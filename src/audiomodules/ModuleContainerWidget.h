/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MODULE_CONTAINER_WIDGET_H
#define MODULE_CONTAINER_WIDGET_H

#include <QFrame>
#include <QEvent>

class AudioModule;

/**
    ModuleContainerWidget contains an interface for one individual
    AudioModule in a signal chain. It communicates with the
    ChainContainerWidget to enable changes in the arrangement of
    modules. The actual parameters of the AudioModule are accessed
    using the widget provided by the module's implementation of
    makeGUI(), which becomes a child of the ModuleContainerWidget.
 */
class ModuleContainerWidget : public QFrame
{
    Q_OBJECT

public:
    ModuleContainerWidget(QWidget* parent, AudioModule* module);

private:
    AudioModule* mAudioModule = nullptr;
};

#endif // MODULE_CONTAINER_WIDGET_H