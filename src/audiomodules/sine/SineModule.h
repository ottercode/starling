/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SINE_MODULE_H
#define SINE_MODULE_H

#include <cstdint>
#include <QObject>
#include <audiocommon/AudioModule.h>

/*
    SineModule is a simple sine wave oscillator which plays notes using
    an ADSR envelope. It was mainly implemented for debugging purposes,
    very early in the project's development. It may or may not be kept
    once we start seriously developing the built-in audio modules.
 */
class SineModule : public AudioModule
{
    Q_OBJECT

public:
    void generateSamples(StBuffers& inBuffers, StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI) override;
    void reset() override;
    QWidget* makeGUI(QWidget* parent) override;

public slots:
    void setAttack(int value);
    void setDecay(int value);
    void setSustain(int value);
    void setRelease(int value);

private:
    double adsr(double time);
    double mAttack = 0.01;
    double mDecay = 0.15;
    double mSustain = 0.30;
    double mRelease = 0.45;

    uint8_t mNote = 69;
    bool isNoteOn = false;
    double mPhase = 0.0;
    double mTime = 1000.0;
};

#endif // SINE_MODULE_H