/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MODULE_REGISTRY_H
#define MODULE_REGISTRY_H

#include <memory>
#include <QAbstractItemModel>
#include <map>
#include <audiocommon/AudioModule.h>
#include <qabstractitemmodel.h>

class ModuleMaker
{
public:
    virtual std::shared_ptr<AudioModule> makeModule() = 0;
    virtual QString getDisplayName() = 0;
    virtual QString getInternalName() = 0;
};

/**
    ModuleRegistry tracks every type of audio module that is available,
    and provides an interface for listing and instantiating each of
    them. ModuleRegistry is a subclass of QAbstractItemModel, allowing
    it to be used with Qt's standard item views. It supports
    drag-and-drop, using the custom "application/starling.module.list"
    MIME type. There should only ever be one instance of ModuleRegistry,
    which is tracked using a static variable, and can be accessed
    using ModuleRegistry::instance().
 */
class ModuleRegistry : public QAbstractItemModel
{
    Q_OBJECT

public:
    ModuleRegistry(QObject* parent = nullptr);
    void populate();
    std::shared_ptr<AudioModule> makeModule(QString internalName);
    static ModuleRegistry* instance();

    // ItemModel functionality
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    QStringList mimeTypes() const override;
    QMimeData* mimeData(const QModelIndexList& indices) const override;
    

private:
    struct Item
    {
        bool isDirectory = false;
        std::unique_ptr<ModuleMaker> moduleMaker;
        QString name;
        Item* parent = nullptr;
        std::vector<std::unique_ptr<Item>> children;
    };

    void addModuleMaker(std::unique_ptr<ModuleMaker>&& moduleMaker, Item* parent = nullptr);
    void addDirectory(QString name, Item* parent = nullptr);

    Item mRootItem;
    std::map<QString, Item*> mModuleMap;
};

template<class T>
class TemplateModuleMaker : public ModuleMaker
{
public:
    TemplateModuleMaker(QString displayName, QString internalName){
        mDisplayName = displayName;
        mInternalName = internalName;
    }

    virtual std::shared_ptr<AudioModule> makeModule(){
        return std::make_shared<T>();
    }

    virtual QString getDisplayName(){
        return mDisplayName;
    }

    virtual QString getInternalName(){
        return "builtin:"+mInternalName;
    }

private:
    QString mDisplayName;
    QString mInternalName;
};

#endif // MODULE_REGISTRY_H