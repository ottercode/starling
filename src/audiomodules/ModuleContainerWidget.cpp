/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ModuleContainerWidget.h"
#include <QPaintEvent>
#include <QPainter>
#include <QBoxLayout>
#include <audiocommon/AudioModule.h>

ModuleContainerWidget::ModuleContainerWidget(QWidget* parent, AudioModule* module) : QFrame(parent)
{
    this->setFrameShape(QFrame::StyledPanel);
    this->setFrameShadow(QFrame::Raised);
    this->setMinimumWidth(20);
    this->setMinimumHeight(20);

    this->setLayout(new QBoxLayout(QBoxLayout::LeftToRight, this));
    auto* moduleGUI = module->makeGUI(this);
    this->layout()->addWidget(moduleGUI);

    mAudioModule = module;
}
