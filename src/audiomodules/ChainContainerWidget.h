/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CHAIN_CONTAINER_WIDGET_H
#define CHAIN_CONTAINER_WIDGET_H

#include <QFrame>
#include <QEvent>

class AudioModuleChain;

/**
    ChainContainerWidget is a custom widget which provides an interface
    for editing an entire signal chain. It manages adding and arranging
    modules, and updates the underlying AudioModuleChain when changes
    occur to the arrangement of modules. The interface for each
    individual module is contained in a ModuleContainerWidget.
 */
class ChainContainerWidget : public QFrame
{
    Q_OBJECT

public:
    ChainContainerWidget(QWidget* parent);
    void setModuleChain(AudioModuleChain* chain);

    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

private:
    AudioModuleChain* mChain = nullptr;
};

#endif // CHAIN_CONTAINER_WIDGET_H