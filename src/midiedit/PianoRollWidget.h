/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PIANO_ROLL_WIDGET_H
#define PIANO_ROLL_WIDGET_H

#include <QGraphicsView>
#include <QPixmap>
#include <audiocommon/MIDI.h>
#include <cstdint>
#include <QWidget>
#include <audiocommon/MIDIClip.h>

class MIDIClip;

/**
    PianoRollWidget is a custom widget which provides an interface for
    viewing and editing MIDI information contained within a MIDIClip.
 */
class PianoRollWidget : public QGraphicsView
{
    Q_OBJECT

public:
    PianoRollWidget(QWidget* parent);
    void setClipAndWidget(MIDIClip* clip, QWidget* clipWidget);
    int noteHeight() const;
    int pixelsPerBeat() const;
    void editNote(uint64_t index, MIDIClip::Note& newNote);
    void removeNote(uint64_t index);

protected:
    void resizeEvent(QResizeEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;

private:
    int mNoteHeight = 16;
    int mPixelsPerBeat = 100;
    MIDIClip* mClip = nullptr;
    QWidget* mClipWidget = nullptr;
};

#endif // PIANO_ROLL_WIDGET_H