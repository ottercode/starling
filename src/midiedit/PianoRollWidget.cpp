/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PianoRollWidget.h"
#include <QGraphicsScene>
#include <guicommon/GridPixmap.h>
#include <audiocommon/MIDIClip.h>
#include <QResizeEvent>
#include "NoteItem.h"

PianoRollWidget::PianoRollWidget(QWidget* parent)
{
    setParent(parent);
    setScene(new QGraphicsScene());

    QSizePolicy policy;
    policy.setHorizontalPolicy(QSizePolicy::Expanding);
    policy.setVerticalPolicy(QSizePolicy::Expanding);
    this->setSizePolicy(policy);

    setBackgroundBrush(createGridPixmap(10, mNoteHeight, Qt::black, Qt::transparent));

    setSceneRect({0, 0, (qreal) width(), mNoteHeight*128.0});
}

void PianoRollWidget::setClipAndWidget(MIDIClip* clip, QWidget* clipWidget)
{
    mClipWidget = clipWidget;
    mClip = clip;

    for(auto& note : *(mClip->getNotes()))
        this->scene()->addItem(new NoteItem(this, note.second, note.first));
}

int PianoRollWidget::noteHeight() const
{
    return mNoteHeight;
}

int PianoRollWidget::pixelsPerBeat() const
{
    return mPixelsPerBeat;
}

void PianoRollWidget::editNote(uint64_t index, MIDIClip::Note& newNote)
{
    mClip->editNote(index, newNote);
    if(mClipWidget)
        mClipWidget->update();
}

void PianoRollWidget::removeNote(uint64_t index)
{
    mClip->removeNote(index);
    if(mClipWidget)
        mClipWidget->update();
}

void PianoRollWidget::resizeEvent(QResizeEvent* event)
{
    setSceneRect({0, 0, (qreal) event->size().width(), mNoteHeight*128.0});
}

void PianoRollWidget::mousePressEvent(QMouseEvent* event)
{
    QGraphicsView::mousePressEvent(event);
    if(!event->isAccepted())
    {
        MIDIClip::Note note;
        auto pos = mapToScene(event->pos());
        note.onTime = pos.x()/(double)mPixelsPerBeat;
        note.offTime = note.onTime + 0.5;
        note.note = 128 - pos.y()/(double)mNoteHeight;

        uint64_t index = mClip->addNote(note);
        this->scene()->addItem(new NoteItem(this, note, index));
        if(mClipWidget)
            mClipWidget->update();
    }
}
