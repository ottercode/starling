/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "NoteItem.h"
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneHoverEvent>
#include <QGraphicsWidget>
#include <QApplication>
#include "PianoRollWidget.h"

NoteItem::NoteItem(PianoRollWidget* pianoRoll, const MIDIClip::Note& note, uint64_t noteID)
{
    mPianoRoll = pianoRoll;
    mNote = note;
    mNoteID = noteID;
    this->setAcceptHoverEvents(true);

    auto pixelsPerBeat = mPianoRoll->pixelsPerBeat();
    auto noteHeight = mPianoRoll->noteHeight();
    this->setX(mNote.onTime * pixelsPerBeat);
    this->setY((127 - mNote.note) * noteHeight);
    this->setFlags(ItemIsSelectable | ItemIsFocusable);
}

QRectF NoteItem::boundingRect() const
{
    auto pixelsPerBeat = mPianoRoll->pixelsPerBeat();
    auto noteHeight = mPianoRoll->noteHeight();
    int width = (mNote.offTime - mNote.onTime) * pixelsPerBeat;

    switch(mCurrentDragAction)
    {
    case DRAG_RESIZE_LEFT:
        width -= mDragOffset;
        break;
    case DRAG_RESIZE_RIGHT:
        width += mDragOffset;
        break;
    default:
        break;
    }

    return QRectF(0, 0, width, noteHeight);
}

void NoteItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    QRectF bounds = boundingRect();
    auto palette = QApplication::palette();
    painter->fillRect(bounds.toRect(), (this->isSelected()) ? palette.highlight() : palette.dark());
    painter->setPen(palette.color(QPalette::Shadow));
    painter->drawRect(bounds.toRect());
}

void NoteItem::hoverMoveEvent(QGraphicsSceneHoverEvent * event)
{
    QRectF bounds = boundingRect();
    if(mousePosIsAtLeft(event->pos(), bounds) || mousePosIsAtRight(event->pos(), bounds))
        this->setCursor(Qt::SizeHorCursor);
    else
        this->unsetCursor();
}

void NoteItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    if(mCurrentDragAction != DRAG_NONE)
    {
        QRectF bounds = boundingRect();
        mDragOffset = this->mapToScene(event->pos()).x() - mDragInitialMouseX;

        switch(mCurrentDragAction)
        {
        case DRAG_RESIZE_LEFT:
        case DRAG_MOVE_WHOLE:
            this->setX(mDragInitialX + mDragOffset);
            if(this->x() < 0)
                this->setX(0);
            break;
        default:
            break;
        }

        mPianoRoll->scene()->invalidate();
    }
}

void NoteItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    QRectF bounds = boundingRect();

    // Check if we should start dragging one of the size
    if(mousePosIsAtLeft(event->pos(), bounds))
        mCurrentDragAction = DRAG_RESIZE_LEFT;
    else if(mousePosIsAtRight(event->pos(), bounds))
        mCurrentDragAction = DRAG_RESIZE_RIGHT;
    else
    {
        mCurrentDragAction = DRAG_MOVE_WHOLE;
        this->setCursor(Qt::DragMoveCursor);
    }
    
    mDragInitialMouseX = this->mapToScene(event->pos()).x();
    mDragInitialX = this->x();
    mDragOffset = 0.0;

    QGraphicsItem::mousePressEvent(event);
}

void NoteItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    double pixelsPerBeat = mPianoRoll->pixelsPerBeat();
    double dragDist = mDragOffset / pixelsPerBeat;
    MIDIClip::Note note = mNote;

    switch(mCurrentDragAction)
    {
    case DRAG_RESIZE_LEFT:
        note.onTime += dragDist;
        break;
    case DRAG_RESIZE_RIGHT:
        note.offTime += dragDist;
        break;
    case DRAG_MOVE_WHOLE:
        note.onTime += dragDist;
        note.offTime += dragDist;
        break;
    default:
        break;
    }

    if(note.onTime < 0.0)
        note.onTime = 0.0;

    if(note.onTime < note.offTime)
    {
        mNote = note;
        mPianoRoll->editNote(mNoteID, mNote);
    }

    mCurrentDragAction = DRAG_NONE;
    mDragOffset = 0;

    this->setAcceptHoverEvents(true);
    mPianoRoll->scene()->invalidate();

    QGraphicsItem::mouseReleaseEvent(event);
}

void NoteItem::keyPressEvent(QKeyEvent* event)
{
    if(event->key() == Qt::Key_Delete)
        this->remove();
}

bool NoteItem::mousePosIsAtLeft(const QPointF& position, const QRectF& bounds)
{
    return (position.x() - bounds.x() < BORDER_GRAB_SIZE);
}

bool NoteItem::mousePosIsAtRight(const QPointF& position, const QRectF& bounds)
{
    return (position.x() - bounds.x() > bounds.width() - BORDER_GRAB_SIZE);
}

void NoteItem::remove()
{
    mPianoRoll->removeNote(mNoteID);
    delete this;
}
