/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTE_ITEM_H
#define NOTE_ITEM_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QStyleOption>
#include <audiocommon/MIDIClip.h>

class PianoRollWidget;

/**
    NoteItem is a QGraphicsItem which represents a single MIDI note. It
    is intended to belong to a PianoRollWidget, and will call on its owner
    when the user interacts with it in ways that should change the
    underlying note data.
 */
class NoteItem : public QGraphicsItem
{
public:
    NoteItem(PianoRollWidget* pianoRoll, const MIDIClip::Note& note, uint64_t nodeID);

    QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

protected:
    // Qt events
    void hoverMoveEvent(QGraphicsSceneHoverEvent* event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;

private:
    bool mousePosIsAtLeft(const QPointF& position, const QRectF& bounds);
    bool mousePosIsAtRight(const QPointF& position, const QRectF& bounds);
    void remove();

    const int BORDER_GRAB_SIZE = 6;

    enum DragAction     // not to be confused with QT's drag-and-drop system
    {                   // these are the actions that can be happening while the mouse is held and dragged
        DRAG_NONE,
        DRAG_RESIZE_LEFT,
        DRAG_RESIZE_RIGHT,
        DRAG_MOVE_WHOLE,
        DRAG_MAX_ENUM
    };

    PianoRollWidget* mPianoRoll = nullptr;
    MIDIClip::Note mNote;
    uint64_t mNoteID;
    DragAction mCurrentDragAction;
    double mDragInitialMouseX = 0.0;
    double mDragInitialX = 0.0;
    double mDragOffset = 0.0;
};

#endif // NOTE_ITEM_H