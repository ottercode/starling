/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.0

Rectangle {
    property var _lane
    property var _backend
    property var _signalChain

    id: laneheader
    width: 100
    height: 64
    color: "lightblue"
    border.color: "black"
    border.width: 1
    anchors.left: (parent) ? parent.left : undefined
    anchors.right: (parent) ? parent.right : undefined

    MouseArea {
        anchors.fill: parent

        onDoubleClicked: {
            if(_signalChain)
                _signalChain.openEditor();
        }
    }

    MouseArea {
        id: bottomDrag

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 10

        cursorShape: Qt.SizeVerCursor
        drag {
            target: this
            axis: Drag.YAxis
            threshold: 0
            smoothed: false
        }
        onMouseYChanged: {
            if(drag.active){
                parent.height = Math.max(mapToItem(parent, 0, mouseY).y, 20)
                if(_lane)
                    _lane.height = parent.height
            }
        }
    }
}