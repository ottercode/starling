/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.0

Rectangle {
    property var _header
    property var _backend
    property var _signalChain

    property real timeScale: 40
    property real interval: 1
    property bool quantize: true
    property real minBars: 32

    id: lane
    width: (parent) ? Math.max(parent.minWidth, minBars * timeScale) : minBars * timeScale
    height: 64
    color: "lightblue"
    border.color: "black"
    border.width: 1

    MouseArea {
        anchors.fill: parent

        onDoubleClicked: {
            if(_backend) {
                if(quantize) {
                    _backend.makeClip(_signalChain, Math.floor(mouseX / timeScale / interval) * interval)
                }
                else {
                    _backend.makeClip(_signalChain, mouseX / timeScale)
                }
            }
        }
    }

    MouseArea {
        id: bottomDrag

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 10

        cursorShape: Qt.SizeVerCursor
        drag {
            target: this
            axis: Drag.YAxis
            threshold: 0
            smoothed: false
        }
        onMouseYChanged: {
            if(drag.active){
                parent.height = Math.max(mapToItem(parent, 0, mouseY).y, 20)
                if(_header)
                    _header.height = parent.height
            }
        }
    }
}