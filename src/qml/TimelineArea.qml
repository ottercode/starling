/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.13
import QtQuick.Controls 2.13

Rectangle {
    id: root
    width: 640
    height: 480

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        ScrollView {
            id: leftView
            objectName: "leftView"

            SplitView.fillWidth: true
            SplitView.minimumWidth: 200

            clip: true

            ScrollBar.vertical.policy: ScrollBar.AlwaysOff
            ScrollBar.vertical.position: rightView.ScrollBar.vertical.position

            Flickable {
                id: leftFlick
                contentWidth: leftContainer.width
                contentHeight: leftContainer.height

                Column {
                    id: leftContainer
                    objectName: "leftContainer"

                    property int minWidth: leftView.width
                }
            }
        }

        ScrollView {
            id: rightView
            objectName: "rightView"

            SplitView.minimumWidth: 150
            SplitView.preferredWidth: 200
            SplitView.maximumWidth: 300
            
            clip: true

            ScrollBar.vertical.position: leftView.ScrollBar.vertical.position

            Flickable {
                id: rightFlick
                contentWidth: rightContainer.width
                contentHeight: rightContainer.height
                
                Column {
                    id: rightContainer
                    objectName: "rightContainer"
                    width: parent.parent.width
                }
            }
        }
    }
}