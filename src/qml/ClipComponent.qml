/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.0

Rectangle {
    id: clip

    property var _backend

    property real interval: 1
    property bool quantize: true
    property real timeScale: (parent) ? parent.timeScale : 40

    signal openEditor(var target)

    x: (_backend) ? _backend.startTime * timeScale : 0
    width: (_backend) ? (_backend.endTime - _backend.startTime) * timeScale : 0
    implicitHeight: 64
    anchors.top: (parent) ? parent.top : undefined
    anchors.bottom: (parent) ? parent.bottom : undefined
    color: "red"
    radius: 6
    border.color: "black"
    border.width: 1

    Rectangle {
        id: contentContainer
        objectName: "contentContainer"

        anchors.left: parent.left
        anchors.right: parent.right
        y: parent.height*0.1
        height: parent.height*0.8

        property real timeScale: parent.timeScale

        color: "transparent"
    }

    MouseArea {
        id: middrag
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        property real initialX: 0
        property real initialStart: 0

        drag {
            target: this
            axis: Drag.XAxis
            threshold: 0
            smoothed: false
        }
        onPressed: {
            initialX = mapToItem(parent.parent, mouseX, 0).x
            initialStart = _backend.startTime
        }
        onDoubleClicked: {
            _backend.openEditor();
        }
        onMouseXChanged: {
            if(drag.active){
                var parentX = mapToItem(parent.parent, mouseX, 0).x
                var timeLength = _backend.endTime - _backend.startTime
                var unQuantizedPos = initialStart + (parentX - initialX) / timeScale

                _backend.startTime = (quantize) ? Math.round(unQuantizedPos / interval) * interval : unQuantizedPos
                _backend.endTime = _backend.startTime + timeLength
            }
        }
    }

    MouseArea {
        id: rightdrag
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 10
        cursorShape: Qt.SizeHorCursor
        drag {
            target: this
            axis: Drag.XAxis
            threshold: 0
            smoothed: false
        }
        onMouseXChanged: {
            if(drag.active){
                var unQuantizedPos = (mouseX + x) / timeScale + _backend.startTime
                _backend.endTime = (quantize) ? Math.round(unQuantizedPos / interval) * interval : unQuantizedPos

                if(_backend.endTime < _backend.startTime + interval) {
                    _backend.endTime = _backend.startTime + interval
                }
            }
        }
    }

    MouseArea {
        id: leftdrag
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: 10
        cursorShape: Qt.SizeHorCursor
        drag {
            target: this
            axis: Drag.XAxis
            threshold: 0
            smoothed: false
        }
        onMouseXChanged: {
            if(drag.active){
                var unQuantizedPos = (mouseX + x) / timeScale + _backend.startTime
                _backend.startTime = (quantize) ? Math.round(unQuantizedPos / interval) * interval : unQuantizedPos

                if(_backend.startTime > _backend.endTime - interval) {
                    _backend.startTime = _backend.endTime - interval
                }
            }
        }
    }
}
