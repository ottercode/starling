/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QtWidgets>
#include <QVBoxLayout>
#include <qboxlayout.h>
#include <qpushbutton.h>
#include "StMainWindow.h"
#include "ui_st_main_window.h"
#include "audioengine/AudioEngine.h"
#include "audiomodules/sine/SineModule.h"
#include "audiomodules/distort/DistortionModule.h"
#include "audioengine/AudioModuleChain.h"
#include "timeline/ClipLane.h"
#include "midiedit/PianoRollWidget.h"
#include "audiomodules/ChainContainerWidget.h"
#include "audiomodules/ModuleRegistry.h"
#include "audiocommon/MIDIClip.h"
#include <QQmlContext>
#include <QVariant>
#include <QQmlComponent>
#include <QQuickItem>
#include "timeline/TimelineGUI.h"

StMainWindow::StMainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::StMainWindow)
{
    ui->setupUi(this);

    ui->moduleTreeView->setModel(new ModuleRegistry(this));

    mAudioEngine = new AudioEngine;
    mAudioEngine->initSoundIO();

    mTimelineGUI = new TimelineGUI(ui->timelineQuickWidget, mAudioEngine);

    // slot & signal connections
    connect(mTimelineGUI, &TimelineGUI::openEditor, this, &StMainWindow::openEditor);
    connect(ui->actionAdd_MIDI_Lane, &QAction::triggered, mTimelineGUI, &TimelineGUI::addMIDITrack);
    connect(ui->playButton, &QAbstractButton::pressed, this, &StMainWindow::playButtonPressed);
    connect(ui->stopButton, &QAbstractButton::pressed, this, &StMainWindow::stopButtonPressed);
}

// Opens the proper editor for a given object, if one exists
void StMainWindow::openEditor(QObject* target)
{
    if(auto* clip = qobject_cast<MIDIClip*>(target))
    {
        if(mCurrentLowerEditor != nullptr)
            mCurrentLowerEditor->deleteLater();

        auto* pianoRoll = new PianoRollWidget(ui->lowerFrame);
        mCurrentLowerEditor = pianoRoll;
        ui->lowerFrame->layout()->addWidget(pianoRoll);
        pianoRoll->setClipAndWidget(clip, nullptr);
        connect(pianoRoll, &QObject::destroyed, this, &StMainWindow::objectDestroyed);
    }
    else if(auto* chain = qobject_cast<AudioModuleChain*>(target))
    {
        if(mCurrentLowerEditor != nullptr)
            mCurrentLowerEditor->deleteLater();

        auto* chainContainer = new ChainContainerWidget(ui->lowerFrame);
        ui->lowerFrame->layout()->addWidget(chainContainer);
        mCurrentLowerEditor = chainContainer;
        chainContainer->setModuleChain(chain);
        connect(chainContainer, &QObject::destroyed, this, &StMainWindow::objectDestroyed);
    }
}

void StMainWindow::playButtonPressed()
{
    mAudioEngine->seek(0);
    mAudioEngine->setPlaybackOn(true);
}

void StMainWindow::stopButtonPressed()
{
    mAudioEngine->setPlaybackOn(false);
    mAudioEngine->seek(0);
}

void StMainWindow::objectDestroyed(QObject* object)
{
    if(object != nullptr && object == mCurrentLowerEditor)
        mCurrentLowerEditor = nullptr;
}

StMainWindow::~StMainWindow()
{
    delete mAudioEngine;
    delete ui;
}